import re
import time
import Queue
import sys
import operator
import os.path

class Entry:
    ''' This class represents the log entry'''
    
    def __init__(self, line, verbose=False):
        # the entry is correct by default, some checks are performed
        # more checks can be added
        self.correct = True
        
        if line.strip() == '':
            # empty line is not a correct log entry, skip it
            self.correct = False
            if verbose:
                print 'Empty line, skip it'
        else:
            # first, split the string into the parts using regexp package
            parsed = re.split('"| |- -|\[|\]|\n|HTTP\/1.0|HTTP\/V1.0', line)
            # filter out empty strings from splitted string
            parsed = filter(None, parsed)
            
            if len(parsed) < 7:
                # the correct entry must contain at 7 elements, skip it
                self.correct = False
                if verbose:
                    print 'Corrupted message with %d elements, skip it' % len(parsed)
            else:
                # store the first element as source 
                self.source = parsed[0]
                # store the second element as timestamp for convenience 
                self.timestamp = parsed[1] + ' ' + parsed[2]
                # convert timestamp to seconds to perform comparison
                t = time.strptime(parsed[1], '%d/%b/%Y:%H:%M:%S')
                # store it 
                self.time = time.mktime(t)
                # store the type of request (GET, POST)
                self.request = parsed[3]
                # store the requested resource
                self.resource = parsed[4]
                # store the status of request, here we read the second from the end
                # because number of lelemnts can be different
                self.status = int(parsed[-2])
                self.size = 0
                size = parsed[-1]
                if size.isdigit():
                    self.size = int(size) 
                self.line = line
        
                
    def printout(self):
        print 'Source ', self.source
        print 'Timestamp ', self.timestamp
        print 'Seconds since Epoch', self.time
        print 'Request ', self.request
        print 'Resource ', self.resource
        print 'Status ', self.status
        print 'Size ', self.size
        print 'Line ', self.line
        
        
class Segment:
    ''' This class represents the 1 hour segment
    encapsulating starttime, time in seconds and count'''
    
    def __init__(self, time, timestamp, count = 1):
        self.starttime = time
        self.timestamp = timestamp
        self.count = count
        
    def printout(self):
        print 'Timestamp ', self.timestamp, ' count ', self.count

class Host:
    def __init__(self,entry,verbose=False, filename = 'blocked.txt'):
        self.ip = entry.source
        self.blocked = False
        self.blocking_time = 0
        self.attempts = 0
        self.first_attempt_time = 0
        self.process(entry,verbose,filename)
        
    def process(self,entry,verbose=False, filename = 'blocked.txt'):
        # If access is already blocked
        if self.blocked:
            # Check if block has expired
            if entry.time < self.blocking_time:
                # If not, then log the request to blocked.txt
                if verbose:
                    print 'Blocked request from ', entry.source
                try:
                    f = open(filename, 'a')
                except IOError:
                    print 'Unable to open %s for writing' % (filename)
                    exit(-1)
                    
                f.write(entry.line)
                f.close()
            else:
                # If yes, then allow access
                if verbose:
                    print '5 min timeout expired. Unblocked ', entry.source
                self.blocked = False
                self.blocking_time = 0
        else:
            # Otherwise, check if login attempt
            if 'login' in entry.resource:
                if entry.status == 200:
                    if verbose:
                        print 'Successful login request'
                    self.first_attempt_time = 0
                    self.attempts = 0
                else:
                    if self.attempts == 0:
                        if verbose:
                            print 'First unsuccessful login attempt from %s, time = %d' %(entry.source, entry.time)
                        self.first_attempt_time = entry.time
                    else:
                        if entry.time < self.first_attempt_time + 20:
                            if self.attempts == 2:
                                if verbose:
                                    print '3 unsuccessful logins within 20 sec. Block ', entry.source
                                self.blocked = True
                                self.blocking_time = entry.time + 300
                        else:
                            if verbose:
                                print 'Restart 20 sec timer for unsuccessful logins for ', entry.source
                            self.attempts == 0
                            self.first_attempt_time = entry.time

                    self.attempts += 1
                    

# This function is used to insert the segment into the list of top segments
def insert(top, segment):
    n = len(top)
    for i in range(n):
        # Check first if current segment has enough count to be in the top list
        if segment.count > top[i].count:
            # It seems so. First, shift all segments lying below in list
            for j in range(i, n-1):
                top[i+1] = top[i]
            # finally, insert current segment here
            top[i] = segment
            break

def feature1(entry, hosts_count):
    hosts_count[entry.source] = hosts_count.get(entry.source,0) + 1

def feature2(entry, resource_size):
    resource_size[entry.resource] = resource_size.get(entry.resource,0) + entry.size

def feature1_finalize(hosts_count, verbose = False, filename = 'hosts.txt', num = 10):
    # Sort number of accesses by count
    
    sorted_hosts = sorted(hosts_count.items(), key=operator.itemgetter(1), reverse = True)
    if verbose:
        print 'Top host/ip:'
        for i in range(len(sorted_hosts)):
            if i < num:
                print sorted_hosts[i][0], sorted_hosts[i][1]
    try:
        f = open(filename, 'w')
    except IOError:
        print 'Unable to open %s for writing' % (filename)
        exit(-1)
        
    # Save top 10 to file
    for i in range(len(sorted_hosts)):
        if i < num:
            f.write('%s,%d\n' % (sorted_hosts[i][0], sorted_hosts[i][1]))
    f.close()

def feature2_finalize(resource_size, verbose = False, filename = 'resources.txt', num = 10):
    # Sort by total size 
    resource_sorted = sorted(resource_size.items(), key=operator.itemgetter(1), reverse = True)
    if verbose:
        print 'Top resources:'
        for i in range(len(resource_sorted)):
            if i < num:
                print resource_sorted[i][0], resource_sorted[i][1]
    try:    
        f = open(filename,'w')
    except IOError:
        print 'Unable to open %s for writing' % (filename)
        exit(-1)
        
    # Save top 10 to file
    for i in range(len(resource_sorted)):
        if i < num:
            f.write('%s\n' % (resource_sorted[i][0]))
    f.close()
        
def feature3(entry, segments, top_segments, time_window = 3600, verbose=False):
    # First, extract current time from the latest log entry
    current_time = entry.time

    # Remove from queue all segments for which the latest log entry falls out of time window
    while not segments.empty() and segments.queue[0].starttime <= current_time - time_window:
        s = segments.get()
        if verbose:
            s.printout()
        insert(top_segments, s)
    # For the remaining segments increase count by 1
    for s in segments.queue:
        s.count += 1
    # Start new segment on latest log entry only if there is no segment with such start time
    # or if queue is empty
    if segments.empty() or (not segments.empty() and current_time != segments.queue[-1].starttime):
        segments.put(Segment(entry.time, entry.timestamp))
    
def feature3_finalize(segments, top_segments, verbose=False, filename = 'hours.txt'):
    # First, add all remaining segments from the queue to the top segments   
    while not segments.empty():
        s = segments.get()
        insert(top_segments, s)
    # Finally, print out top segments
    
    try:
        f = open(filename, 'w')
    except IOError:
        print 'Unable to open %s for writing' % (filename)
        exit(-1)
        
    if verbose:
        print 'Top segments: '
    for s in top_segments:
        if s.count != 0:
            f.write('%s,%d\n'%(s.timestamp,s.count))
            if verbose:
                s.printout()
    f.close()

def feature4(hosts,entry,verbose=False,filename = 'blocked.txt'):
    # If host is already in dict them simply process
    # otherwise add it there if login attempt is made
    if entry.source in hosts:
        h = hosts[entry.source]
        h.process(entry,verbose,filename)
    elif 'login' in entry.resource:
        h = Host(entry,verbose,filename) 
        hosts[entry.source] = h

# Check if file can be opened and wipe the old content out        
def check_file(filename):
    try:
        with open(filename, 'w'):
            pass
    except IOError:
        print 'Unable to open %s for writing' % (filename)
        exit(-1)
        
def main():
    # check if correct number of arguments is passed
    if len(sys.argv) != 6:
        print 'To use run the following command: process.py <in:logfile> <out:hosts> <out:hours> <out:resources> <out:blocked>'
        exit(-1)
        
    # parse input 
    infile = sys.argv[1]
    hosts_file = sys.argv[2]
    hours_file = sys.argv[3]
    resources_file = sys.argv[4]
    blocked_file = sys.argv[5]
    
    # Check in advance that files can be opened and remove previous content 
    check_file(hosts_file)
    check_file(hours_file)
    check_file(resources_file)
    check_file(blocked_file)

    # check the log file
    if not os.path.exists(infile):
        print 'Unable to find ', infile
        exit(-1)
        
    # set verbose for debug purpose
    verbose = True
    
    # Number of requests for each host/ip for feature 1: ip/host - key, count - value 
    hosts_count = {}
    # Total sent size for each resource for feature 2: resource - key, size - value 
    resource_size = {}
    
    # Number of top segments for feature 3
    n_segments = 10
    # Time window for feature 3 in seconds
    time_window = 3600
    # Queue for segments
    segments = Queue.Queue()
    # List of top segments with the maximal number of count
    top_segments = [Segment(0,'0', count = 0)] * n_segments

    # List of hosts stored in dictionary(based on hash table) for feature 4
    hosts = {}
        
    with open(infile) as f:
        for line in f:
            # read in and parse log entry
            entry = Entry(line, verbose=verbose)
            
            # process entry if it is correct, otherwise skip it
            if entry.correct:
                feature1(entry, hosts_count)
                feature2(entry, resource_size)
                feature3(entry, segments, top_segments, time_window)
                feature4(hosts,entry, filename = blocked_file, verbose = verbose)
        
    feature1_finalize(hosts_count, verbose = verbose, filename = hosts_file)    
    feature2_finalize(resource_size, verbose = verbose, filename = resources_file)
    feature3_finalize(segments, top_segments, verbose = verbose, filename = hours_file)
    
if __name__ == '__main__':
    main()

